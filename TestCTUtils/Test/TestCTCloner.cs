﻿using CTUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestCTUtils.Entidades;

namespace TestCTUtils.Test
{
    [TestClass]
    public class TestCTCloner
    {
        [TestMethod]
        [DataRow("CamposSimple")]
        [DataRow("CamposConDetalles")]
        public void TestCTClonerSerializable(string Caso)
        {
            ClaseTestSerializable objetoClaseOrigen = ObtenerObjetoAProbarSerializable(Caso);
            ClaseTestSerializable objetoClaseDestino = CTCloner.ClonarSerializable(objetoClaseOrigen);

            objetoClaseDestino.campo1 = 10;
            objetoClaseDestino.campo2 = "10";
            if(objetoClaseDestino.detalles.Count > 0)
            {
                objetoClaseDestino.detalles.FirstOrDefault().campo1 = 20;
                objetoClaseDestino.detalles.FirstOrDefault().campo2 = "20";
            }

            Assert.AreNotEqual(objetoClaseOrigen.campo1, objetoClaseDestino.campo1);
            Assert.AreNotEqual(objetoClaseOrigen.campo2, objetoClaseDestino.campo2);

            if (objetoClaseDestino.detalles.Count > 0)
            {
                Assert.AreNotEqual(objetoClaseOrigen.detalles.FirstOrDefault().campo1, objetoClaseDestino.detalles.FirstOrDefault().campo1);
                Assert.AreNotEqual(objetoClaseOrigen.detalles.FirstOrDefault().campo2, objetoClaseDestino.detalles.FirstOrDefault().campo2);
            }
        }
        public ClaseTestSerializable ObtenerObjetoAProbarSerializable(string Caso)
        {
            if (Caso == "CamposSimple") return ObtenerObjeto_CamposSimpleSerializable();
            if (Caso == "CamposConDetalles") return ObtenerObjeto_CamposYDetallesSerializale();
            else return null;
        }
        public ClaseTestSerializable ObtenerObjeto_CamposSimpleSerializable()
        {
            ClaseTestSerializable objetoClase = new ClaseTestSerializable();
            objetoClase.campo1 = 1;
            objetoClase.campo2 = "1";
            return objetoClase;
        }
        public ClaseTestSerializable ObtenerObjeto_CamposYDetallesSerializale()
        {
            ClaseTestSerializable objetoClase = new ClaseTestSerializable();
            objetoClase.campo1 = 1;
            objetoClase.campo2 = "1";
            objetoClase.detalles = new List<ClaseTestDetalleSerializable>()
            {
                new ClaseTestDetalleSerializable()
                {
                    campo1 = 2,
                    campo2 = "2"
                }
            };
            return objetoClase;
        }


        [TestMethod]
        [DataRow("CamposSimple")]
        [DataRow("CamposConDetalles")]
        public void TestCTClonerNoSerializable(string Caso)
        {
            ClaseTest objetoClaseOrigen = ObtenerObjetoAProbarNoSerializable(Caso);
            ClaseTest objetoClaseDestino = CTCloner.Clonar(objetoClaseOrigen);

            objetoClaseDestino.campo1 = 10;
            objetoClaseDestino.campo2 = "10";
            if (objetoClaseDestino.detalles.Count > 0)
            {
                objetoClaseDestino.detalles.FirstOrDefault().campo1 = 20;
                objetoClaseDestino.detalles.FirstOrDefault().campo2 = "20";
            }

            Assert.AreNotEqual(objetoClaseOrigen.campo1, objetoClaseDestino.campo1);
            Assert.AreNotEqual(objetoClaseOrigen.campo2, objetoClaseDestino.campo2);

            if (objetoClaseDestino.detalles.Count > 0)
            {
                Assert.AreEqual(objetoClaseOrigen.detalles.FirstOrDefault().campo1, objetoClaseDestino.detalles.FirstOrDefault().campo1);
                Assert.AreEqual(objetoClaseOrigen.detalles.FirstOrDefault().campo2, objetoClaseDestino.detalles.FirstOrDefault().campo2);
            }
        }
        public ClaseTest ObtenerObjetoAProbarNoSerializable(string Caso)
        {
            if (Caso == "CamposSimple") return ObtenerObjeto_CamposSimpleNoSerializable();
            if (Caso == "CamposConDetalles") return ObtenerObjeto_CamposYDetallesNoSerializable();
            else return null;
        }
        public ClaseTest ObtenerObjeto_CamposSimpleNoSerializable()
        {
            ClaseTest objetoClase = new ClaseTest();
            objetoClase.campo1 = 1;
            objetoClase.campo2 = "1";
            return objetoClase;
        }
        public ClaseTest ObtenerObjeto_CamposYDetallesNoSerializable()
        {
            ClaseTest objetoClase = new ClaseTest();
            objetoClase.campo1 = 1;
            objetoClase.campo2 = "1";
            objetoClase.detalles = new List<ClaseTestDetalle>()
            {
                new ClaseTestDetalle()
                {
                    campo1 = 2,
                    campo2 = "2"
                }
            };
            return objetoClase;
        }
    }
}
