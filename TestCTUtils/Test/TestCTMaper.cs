using CTUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TestCTUtils.Entidades;

namespace TestCTUtils.Test
{
    [TestClass]
    public class TestCTMaper
    {
        [TestMethod]
        [DataRow("CamposSimple")]
        [DataRow("CamposConDetalles")]
        public void TestCTMapper(string Caso)
        {
            ClaseTest objetoClaseOrigen = ObtenerObjetoAProbar(Caso);
            ClaseTest objetoClaseDestino = CTMapper.Map<ClaseTest, ClaseTest>(objetoClaseOrigen);

            Assert.AreEqual(objetoClaseDestino.campo1, objetoClaseDestino.campo1);
            Assert.AreEqual(objetoClaseDestino.campo2, objetoClaseDestino.campo2);
            Assert.AreEqual(objetoClaseDestino.detalles, objetoClaseDestino.detalles);
        }

        public ClaseTest ObtenerObjetoAProbar(string Caso)
        {
            if (Caso == "CamposSimple") return ObtenerObjeto_CamposSimple(); 
            if (Caso == "CamposConDetalles") return ObtenerObjeto_CamposYDetalles();
            else return null;
        }
        public ClaseTest ObtenerObjeto_CamposSimple()
        {
            ClaseTest objetoClase = new ClaseTest();
            objetoClase.campo1 = 1;
            objetoClase.campo2 = "1";
            return objetoClase;
        }
        public ClaseTest ObtenerObjeto_CamposYDetalles()
        {
            ClaseTest objetoClase = new ClaseTest();
            objetoClase.campo1 = 1;
            objetoClase.campo2 = "1";
            objetoClase.detalles = new List<ClaseTestDetalle>()
            {
                new ClaseTestDetalle()
                {
                    campo1 = 2,
                    campo2 = "2"
                }
            };
            return objetoClase;
        }

    }
}
