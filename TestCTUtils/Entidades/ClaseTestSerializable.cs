﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCTUtils.Entidades
{
    [Serializable]
    public class ClaseTestSerializable
    {
        public ClaseTestSerializable()
        {
            detalles = new List<ClaseTestDetalleSerializable>();
        }

        public int campo1 { get; set; }
        public string campo2 { get; set; }
        public List<ClaseTestDetalleSerializable> detalles { get; set; }
    }
    [Serializable]
    public class ClaseTestDetalleSerializable
    {
        public int campo1 { get; set; }
        public string campo2 { get; set; }
    }
}
