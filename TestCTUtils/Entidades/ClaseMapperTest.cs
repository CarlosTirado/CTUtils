﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCTUtils.Entidades
{
    public class ClaseTest
    {
        public ClaseTest()
        {
            detalles = new List<ClaseTestDetalle>();
        }
        public int campo1 { get; set; }
        public string campo2 { get; set; }
        public List<ClaseTestDetalle> detalles { get; set; }
    }
    public class ClaseTestDetalle
    {
        public int campo1 { get; set; }
        public string campo2 { get; set; }
    }
}
