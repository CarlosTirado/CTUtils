﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace CTUtil
{
    public static class CTCloner
    {
        /// <summary>
        /// Retuns a new instance of object 'obj'.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T Clonar<T>(T obj) where T : new()
        {
            T objResultado = CTMapper.Map<T, T>(obj);
            return objResultado;
        }

        /// <summary>
        /// Retuns a new instance of list object 'obj'.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static List<T> Clonar<T>(List<T> obj) where T : new()
        {
            List<T> nuevaLista = new List<T>();
            obj.ForEach(t => nuevaLista.Add(Clonar(t)));
            return nuevaLista;
        }

        /// <summary>
        /// Retuns a new instance of object 'obj'. The object type of 'obj' must bu a Serializable class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T ClonarSerializable<T>(T obj)
        {
            T objResult;
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                ms.Position = 0;
                objResult = (T)bf.Deserialize(ms);
            }
            return objResult;
        }

        /// <summary>
        /// Retuns a new instance of list object 'obj'. The object type of 'obj' must bu a Serializable class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fuente"></param>
        /// <returns></returns>
        public static List<T> ClonarSerializable<T>(List<T> fuente)
        {
            List<T> nuevaLista = new List<T>();
            fuente.ForEach(t => nuevaLista.Add(ClonarSerializable(t)));
            return nuevaLista;
        }
    }
}
