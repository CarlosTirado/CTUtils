﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CTUtil
{
    public class CTMapper
    {
        /// <summary>
        /// Devuelve un objeto DESTINO, con los campos que coinciden entre los objetos ORIGEN y DESTINO
        /// </summary>
        /// <typeparam name="O"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <param name="Origen"></param>
        /// <param name="Destino"></param>
        public static void Map<O, D>(O Origen, D Destino)
        {

            Type TipoClaseOrigen = Origen.GetType();
            PropertyInfo[] PropiedadesOrigen = TipoClaseOrigen.GetProperties();

            Type TipoClaseDestino = Destino.GetType();
            PropertyInfo[] PropiedadesDestino = TipoClaseDestino.GetProperties();

            foreach (PropertyInfo propiedad_origen in PropiedadesOrigen)
            {
                try
                {
                    PropertyInfo propiedad_destino = PropiedadesDestino.Where(t => t.Name == propiedad_origen.Name).FirstOrDefault();
                    if (propiedad_destino != null && propiedad_origen.PropertyType == propiedad_destino.PropertyType) propiedad_destino.SetValue(Destino, propiedad_origen.GetValue(Origen));
                }
                catch { }
            }
        }

        /// <summary>
        /// Devuelve una lista de objetos DESTINO, con los campos que coinciden entre los objetos ORIGEN y DESTINO
        /// </summary>
        /// <typeparam name="O"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <param name="lOrigen"></param>
        /// <param name="lDestino"></param>
        public static void Map<O, D>(List<O> lOrigen, List<D> lDestino) where D : new()
        {
            foreach (O Origen in lOrigen)
            {
                D Destino = new D();
                Map<O, D>(Origen, Destino);
                lDestino.Add(Destino);
            }
        }

        /// <summary>
        /// Devuelve un objeto DESTINO, con los campos que coinciden entre los objetos ORIGEN y DESTINO
        /// </summary>
        /// <typeparam name="O"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <param name="Origen"></param>
        /// <param name="Destino"></param>
        public static D Map<O, D>(O Origen) where D : new()
        {
            D Destino = new D();

            Type TipoClaseOrigen = Origen.GetType();
            PropertyInfo[] PropiedadesOrigen = TipoClaseOrigen.GetProperties();

            Type TipoClaseDestino = Destino.GetType();
            PropertyInfo[] PropiedadesDestino = TipoClaseDestino.GetProperties();

            foreach (PropertyInfo propiedad_origen in PropiedadesOrigen)
            {
                try
                {
                    PropertyInfo propiedad_destino = PropiedadesDestino.Where(t => t.Name == propiedad_origen.Name).FirstOrDefault();
                    if (propiedad_destino != null && propiedad_origen.PropertyType == propiedad_destino.PropertyType) propiedad_destino.SetValue(Destino, propiedad_origen.GetValue(Origen));
                }
                catch { }
            }
            return Destino;
        }

        /// <summary>
        /// Devuelve una lista de objetos DESTINO, con los campos que coinciden entre los objetos ORIGEN y DESTINO
        /// </summary>
        /// <typeparam name="O"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <param name="lOrigen"></param>
        /// <param name="lDestino"></param>
        public static List<D> Map<O, D>(List<O> lOrigen) where D : new()
        {
            List<D> lDestino = new List<D>();
            foreach (O Origen in lOrigen)
            {
                D Destino = new D();
                Map<O, D>(Origen, Destino);
                lDestino.Add(Destino);
            }
            return lDestino;
        }

        /// <summary>
        /// Devulve un objeto Dynamic con los campos del objeto ORIGEN que no sean nulos
        /// </summary>
        /// <typeparam name="O"></typeparam>
        /// <param name="Origen"></param>
        /// <returns></returns>
        public static dynamic MapDynamic<O>(O Origen)
        {
            dynamic Destino = new ExpandoObject();
            Type TipoClaseOrigen = Origen.GetType();
            PropertyInfo[] PropiedadesOrigen = TipoClaseOrigen.GetProperties();
            foreach (PropertyInfo propiedad_origen in PropiedadesOrigen)
            {
                try
                {
                    if (propiedad_origen.GetValue(Origen) != null || !String.IsNullOrEmpty(propiedad_origen.GetValue(Origen).ToString()))
                    {
                        ((IDictionary<String, Object>)Destino).Add(propiedad_origen.Name, propiedad_origen.GetValue(Origen));
                    }
                }
                catch { }
            }
            return Destino;
        }
    }
}
